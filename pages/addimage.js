import Head from 'next/head'
import { makeStyles } from '@material-ui/core/styles';
import {Fab, Grid, Button, Input, InputLabel, Typography } from "@material-ui/core";
import ImageIcon from '@material-ui/icons/Image';
import { useState } from 'react'
import Canvas from '../components/Canvas'
import axios from 'axios'
import SendIcon from '@material-ui/icons/Send';
import FormData from 'form-data'

const useStyles = makeStyles({
  hidden: {
    display: "none",
  },
  importLabel: {
    color: "black",
    justify: 'center',
  },
  container: {
    marginTop: 50,
  },
  container2: {
    marginTop: 30
  },
  image: {
    height: '45%',
    width: '50%',
    display: 'none',
  },
  canvas: {
    height: '500px',
    width: '1000px',
  }
});

export default function AddImage() {
  const classes = useStyles();
  const [Imagename, setImagename] = useState('./images/no_file_selected.png')
  const [ImageObj, setImageObj] = useState(null)
  const [fileName, setFileName] = useState('No file Selected.')
  const [Boxes, setBoxes] = useState([])

  function getAnswer(data) {
    let answer, max;
    max = 0;
    let total=0;
    let probabilty =1;
    for (const key in data) {
      if (data[key] > max) {
        answer = key;
        max = data[key];
        total += data[key]; 
      }
    }
    if (total !== 0) {
      probabilty = max/total;
      }
    if (max === 0) {
        return {"answer": 'I have failed you',
                "probabilty": probabilty
                } 
      }
    return {"answer": answer,
            "probabilty": probabilty
          }
  }

  function handleChange(value) {
    let file = URL.createObjectURL(event.target.files[0])
    setImageObj(event.target.files[0])
    let splitVals = value.split('\\');
    setFileName(splitVals[splitVals.length-1]);
    setImagename(file)
    setBoxes([])
  }
  
  function handleClick(value) {
    alert('Agent Chanda is working and will respond soon!!')
    let formData = new FormData()
    formData.append('image', ImageObj)
    let data = {
      "len": Boxes.length - 1,
      "coordinates": Boxes.slice(1, Boxes.length),
      "ratio": Boxes[0]
    }
    const dataJson = JSON.stringify(data);
    formData.append('data', dataJson)
    axios.post('http://peerconnected.com:5000/api/upload/image', formData, {
      headers: {
        'accept': 'application/json',
        'Accept-Language': 'en-US,en;q=0.8',
        'Content-Type': 'multipart/form-data',//; boundary=${formData._boundary}`,
      }
    })
    .then(function (response) {
      let result = getAnswer(response.data.count)
      if (result['answer'] === 'I have failed you')
        alert(result['answer'])
      else
        alert("The answer is "+ result['answer']+  " and its probabilty is "+ result['probabilty']*100+" %")
    })
    .catch(function (error) {
      alert("Hey its seem my backend broke, I am very buggy : "+ error)
    })
  }

  return (
    <div>
        <Head>
            <title>Agent Chanda . Add Image</title>
            <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        </Head>
        <Grid container spacing={3} className={classes.container} >
        <Grid item xs={12} sm={12} >
          <Canvas Image={Imagename} Boxes={Boxes}/>
        </Grid>
        <Grid item xs={6} sm={3} container alignContent="center" justify="center">
          <label htmlFor="upload-photo">
              <input
                style={{ display: 'none' }}
                id="upload-photo"
                name="upload-photo"
                type="file"
                accept="image/*"
                onChange={(e) =>handleChange(e.target.value)}
              />
              <Fab
                size="small"
                component="span"
                aria-label="add"
                variant="extended"
              >
                <ImageIcon /> Upload photo
              </Fab>
            </label>
        </Grid>
        <Grid item xs={6} sm={5} container alignContent="center" justify="center">
          <Typography variant="h6" gutterBottom className={classes.info}> {fileName} </Typography>
        </Grid>
        <Grid item xs={6} sm={4}>
        <label htmlFor="upload-data">
              <button
                style={{ display: 'none' }}
                id="upload-data"
                name="upload-data"
                onClick={(e) =>handleClick(e.target.value)}
              />
              <Fab
                size="small"
                component="span"
                aria-label="add"
                variant="extended"
              >
                <SendIcon /> Ask Agent Chanda
              </Fab>
            </label>
        </Grid>
      </Grid>         
    </div>
  )
}