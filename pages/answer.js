import Head from 'next/head'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
  },  
});

export default function Home() {
  const classes = useStyles();
  return (
    <div>
      <Head>
        <title>Agent Chanda . Answer</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
    </div>
  )
}
