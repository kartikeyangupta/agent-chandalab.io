import Head from 'next/head'
// import Intro from '../components/Intro'
import Example from '../components/example'
import { makeStyles } from '@material-ui/core/styles';
import NewCanvas from '../components/NewCanvas';

const useStyles = makeStyles({
  root: {
  },
  
});

export default function Example1() {
  const classes = useStyles();
  return (
    <div>
      <Head>
        <title>Agent Chanda . Example</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <NewCanvas />
    </div>
  )
}
