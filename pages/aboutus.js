import Head from 'next/head'
import Info from '../components/Info'
import FrontCards from '../components/FrontCards'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
  },
});

export default function AboutUs() {
  const classes = useStyles();
  return (
    <div>
        <Head>
            <title>Agent Chanda . About Us</title>
            <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        </Head>
        <Info />
    </div>
  )
}