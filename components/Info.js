import AppBar from '@material-ui/core/AppBar';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import theme from '../src/theme';
import Link from 'next/link'



const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    heading: {
        flexGrow: 1,
        textAlign: 'center',
        marginTop: 100,
        marginBottom: 100,
    },
    heading: {
        flexGrow: 1,
        textAlign: 'center',
        marginTop: 100,
    },
    image: {
        height: '60%',
        width: '60%',
    },

  }));


export default function Info() {
    const classes = useStyles(theme);
    return (
        <div>
            <Typography variant="h2" component="h1" gutterBottom className={classes.heading}> How I Work </Typography>
            <Grid container alignContent="center" justify="center">
                <img className={classes.image} src='./images/how_I_work.png'></img>
            </ Grid>
            <Typography variant="h2" component="h1" gutterBottom className={classes.heading}> Demo Video </Typography>
            <Grid container alignContent="center" justify="center">
                {/* <img className={classes.image} src='./images/how_I_work.png'></img> */}
                <iframe width="420" height="315"
                    src="https://www.youtube.com/embed/gUVAMnPGkmY?autoplay=1">
                </iframe>
            </ Grid>
            <Typography variant="h2" component="h1" gutterBottom className={classes.heading}> My Developers </Typography>
            <Grid container alignContent="center" justify="center">
                <img className={classes.image} src='./images/my_creator.png'></img>
            </ Grid>
            <Typography variant="h2" component="h1" gutterBottom className={classes.heading}> My Technology Stack </Typography>
            <Grid container alignContent="center" justify="center">
                <img className={classes.image} src='./images/technology_stack.png'></img>
            </ Grid>
        </div>
    )
}