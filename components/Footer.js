import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles({
    mainfooter: {
        color: 'white',
        backgroundColor: '#000000',
        paddingTop: 3,
        marginTop: 80,
        position: 'relative',
        marginBottom: 0,
        alignItems:"flex-end",
    },
    footerText: {
        textAlign: 'center',
    },
    aLink: {
        color: 'purple',
    }
  });


export default function Footer() {
    const classes = useStyles();
    return (
        <Grid container justify='flex-start' className={classes.mainfooter}>
            <p className={classes.footerText}>
                &copy;{new Date().getFullYear()} AGENT CHANDA | All rights reserved |&#160;
                <a className={classes.aLink} href="mailto:kartikeyangupta123@gmail.com"> Contact </a> &#160; Developers.
            </p>
        </Grid>
  );
}
