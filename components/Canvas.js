import { makeStyles } from '@material-ui/core/styles';
import React, { useState, useEffect, useRef, useLayoutEffect } from "react";

const useStyles = makeStyles({
    hidden: {
      display: "none",
    },
    importLabel: {
      color: "black",
      justify: 'center',
    },
    container: {
      marginTop: 50
    },
    container2: {
      marginTop: 30
    },
    image: {
      height: '45%',
      width: '50%',
      display: 'none',
    },
    canvas: { 
      border: '1px solid black',
      marginLeft: '100px',
    }
  });

export default function Canvas( {Image, Boxes} ) {
    const classes = useStyles();
    const canvas = useRef(null);
    const image = useRef(null);
    
    var rect =  {},
        drag = false,
        flag = false
        //boxes = []

    function mouseDown(e) {
        rect.startX =  e.pageX - canvas.current.offsetLeft;
        rect.startY =  e.pageY - canvas.current.offsetTop;
        drag = true;
        flag = true;
    }
    
    function mouseUp() {
        drag = false;
        if (flag) {
          Boxes.push([rect.startX, rect.startY, rect.w, rect.h]);
          flag = false;
        }
    }
    
    function mouseMove(e) {
        const ctx = canvas.current.getContext("2d")
        if (drag) {
          rect.w = (e.pageX - canvas.current.offsetLeft) - rect.startX;
          rect.h = (e.pageY - canvas.current.offsetTop) - rect.startY ;
          ctx.clearRect( 0, 0,canvas.current.width, canvas.current.height);
          draw();
        }
    }

    function touchStart(e) {
      rect.startX =  e.pageX - canvas.current.offsetLeft;
      rect.startY =  e.pageY - canvas.current.offsetTop;
      drag = true;
      flag = true;
  }
  
  function touchEnd() {
      drag = false;
      if (flag) {
        Boxes.push([rect.startX, rect.startY, rect.w, rect.h]);
        flag = false;
      }
  }
  
  function touchMove(e) {
      const ctx = canvas.current.getContext("2d")
      if (drag) {
        rect.w = (e.pageX - canvas.current.offsetLeft) - rect.startX;
        rect.h = (e.pageY - canvas.current.offsetTop) - rect.startY ;
        ctx.clearRect( 0, 0,canvas.current.width, canvas.current.height);
        draw();
      }
  }

    function draw() {
        const ctx = canvas.current.getContext("2d")
        ctx.drawImage(image.current, 0, 0, canvas.current.width, canvas.current.height)
        if (Boxes.length === 1) {
          ctx.strokeStyle = "#FF0000";
          ctx.strokeRect(rect.startX, rect.startY, rect.w, rect.h);  
        }
        else {
          ctx.strokeStyle = "#006400";
          ctx.strokeRect(rect.startX, rect.startY, rect.w, rect.h);
        }

        if (Boxes.length > 1) {
          for (let i=0; i<Boxes.length; i++){
            if (i === 1) {
              ctx.strokeStyle = "#FF0000";
              ctx.strokeRect(Boxes[i][0], Boxes[i][1], Boxes[i][2], Boxes[i][3]);  
            }
            else {
              ctx.strokeStyle = "#006400";
              ctx.strokeRect(Boxes[i][0], Boxes[i][1], Boxes[i][2], Boxes[i][3]);
            }
          }
        }
      }

    useEffect(() => {
        image.current.onload = () => {
          let cw, ch, ratio;
          if (image.current.width > window.innerWidth) {
            ratio = (image.current.width / (window.innerWidth * 0.8));
            cw = image.current.width/ratio;
            ch = image.current.height/ratio;
          }
          else {
            ratio = 1
            cw = image.current.width;
            ch = image.current.height;
          }
          canvas.current.width = cw
          canvas.current.height = ch
          const ctx = canvas.current.getContext("2d")
          Boxes.push(ratio)
          ctx.drawImage(image.current, 0, 0, canvas.current.width, canvas.current.height)  
        }
        });

      return(
        <div >
          <canvas ref={canvas} className={classes.canvas} 
                  onMouseDown={mouseDown} onMouseUp={mouseUp} onMouseMove={mouseMove} 
                  onTouchStart={touchStart} onTouchMove={touchMove} onTouchEnd={touchEnd} />
          <img ref={image} src={Image} className={classes.image}/>
        </div>
      )
    }