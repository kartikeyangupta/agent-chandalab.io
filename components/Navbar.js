import AppBar from '@material-ui/core/AppBar';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import theme from '../src/theme';
import Link from 'next/link'

const useStyles = makeStyles((theme) => ({

    root: {
      flexGrow: 1,
    },
    appBar: {
        background : '#121212',
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    logo: {
        flexGrow: 1,
        width: 30,
        height: 30,
    },
    button: {
      fontSize: '1rem',
    }
  }));

export default function Navbar() {
    const classes = useStyles(theme);

    return (
        <div>
             <AppBar position="static" className={classes.appBar}>
                <Toolbar>
                  <Link href="/">
                      <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <img src='logo.png' className={classes.logo}></img>  
                      </IconButton>
                    </Link>
                    <Link href="/">
                      <Typography variant="h6" className={classes.title}>
                        Agent-Chanda
                      </Typography>
                    </Link>
                    <Link href="/addimage"><Button color="inherit" className={classes.button}>Add Image</Button></Link>
                    <Link href="/addquestion"><Button color="inherit" className={classes.button}>Add Text</Button></Link>
                    <Link href="/aboutus"><Button color="inherit" className={classes.button}>About Me</Button></Link>
                    <Link href="/login"><Button color="inherit" className={classes.button}>Login</Button></Link>
                </Toolbar>
            </AppBar>
      </div>
    )
}