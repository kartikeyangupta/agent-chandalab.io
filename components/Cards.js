import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ImageIcon from '@material-ui/icons/Image';
import TextFieldsIcon from '@material-ui/icons/TextFields';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import Link from 'next/link';

const useStyles = makeStyles({
  root: {
    minWidth: 125,
    float: "left",
    marginRight: 10,
    justifyContent: 'center'
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function Cards( { cardName, cardType, linkToButton } ) {
  const classes = useStyles();

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent>
        <Typography variant="h6" component="h2">
          { cardName }
        </Typography>
      </CardContent>
      <CardActions  style={{justifyContent: 'center'}} >
            <Button size="small"  justify="flex-end" href={linkToButton}>
                {
                cardType === 'image' ? <ImageIcon fontSize='large' /> : [ cardType === 'text' ? <TextFieldsIcon fontSize='large' /> : <CameraAltIcon fontSize='large' /> ]
                } 
            </Button>
      </CardActions>
    </Card>
  );
}