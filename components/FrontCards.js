import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Cards from '../components/Cards'
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles({
  root: { 
  },
  boxes: {
      marginTop:100,
  }
});

export default function FrontCards() {
  const classes = useStyles();
  
  return (
    <div className={classes.boxes}>
        <Grid container alignContent="center" justify="center">
            <Cards cardName='Add Image' cardType='image' linkToButton='/addimage' />
            <Cards cardName='Add Question' cardType='text' linkToButton='/addquestion' />
            <Cards cardName='Click Image' cardType='camera' linkToButton='/clickimage' />
        </Grid>
    </div>
  );
}