import AppBar from '@material-ui/core/AppBar';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import theme from '../src/theme';
import Link from 'next/link'



const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    heading: {
        flexGrow: 1,
        textAlign: 'center',
        marginTop: 100,
        marginBottom: 100,
    },
    info: {
        flexGrow: 1,
        textAlign: 'center',
        marginTop: 50,
    },
  }));


export default function Intro() {
    const classes = useStyles(theme);
    const introHeading = 'Agent Chanda'
    return (
        <div>
            <Typography variant="h1" component="h2" gutterBottom className={classes.heading}> {introHeading}</Typography>
            <Typography variant="h4" gutterBottom className={classes.info}>Hi! Agent Chanda at your service.</Typography>
            <Typography variant="h5" gutterBottom className={classes.info}>I will help you find answers to your MCQs questions in clicks.</Typography>
            <Typography variant="h6" gutterBottom className={classes.info}>Currently I am at my beta version 1, to know more about me check the <Link href="/aboutus"><a>About me</a></Link> Section.</Typography>
        </div>
    )
}