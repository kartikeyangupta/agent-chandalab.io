
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import theme from '../src/theme';
import Link from 'next/link'
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    heading: {
        flexGrow: 1,
        textAlign: 'center',
        marginTop: 100,
        marginBottom: 100,
    },
    info: {
        flexGrow: 1,
        textAlign: 'center',
        marginTop: 50,
    },
    image: {
        height: '30%',
        width: '30%',
    },
  }));


export default function WIP() {
    const classes = useStyles(theme);
    const Heading = 'Work in Progess'
    const version = "Beta 2.0"
    const Description = 'This Page in Under Construction, and will be available by version : ' + version
    return (
        <div>
            <Typography variant="h1" component="h2" gutterBottom className={classes.heading}> {Heading}</Typography>
            <Grid container alignContent="center" justify="center">
                <img className={classes.image} src='/images/pages-output/work_in_progress.png'></img>
            </ Grid>
            <Typography variant="h6" gutterBottom className={classes.info}> {Description}, <Link href="/"><a>Back to Home</a></Link>.</Typography>
        </div>
    )
}